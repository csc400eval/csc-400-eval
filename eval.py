from flask import Flask, render_template, request
import pymongo 
from pymongo import MongoClient

server = "ds049661.mongolab.com"
port = 49661
db_name = "grantsystem"
username = "csc400"
password = "csc400"

conn = MongoClient(server, port)
db = conn[db_name]
db.authenticate(username, password)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'hard to guess string'


@app.route('/', methods=['GET', 'POST'] )
def Home():
    return render_template('Home_Page.html')

@app.route('/faculty', methods=['GET', 'POST'] )
def Faculty():
    posts = db.posts
    facultyId = request.form.get("facID","");
    facultyId = int(facultyId) if facultyId else ""
    l= []
    for post in posts.find({'Applicants':{'$exists': True }}):
        for i in range(posts.aggregate([{"$match":{"_id":post['_id']}},{'$unwind':"$Applicants"},{'$group':{'_id':"$_id", 'number':{'$sum':1}}}])['result'][0]['number']):
            for post2 in posts.find({"GrantType" : "Faculty" , "Applicants.appId" : post['Applicants'][i-1]['appId']}, { "Applicants.$.Evaluators" : 1 }):
                try:
                    num = len(post2["Applicants"][0]["Evaluators"])
                except:
                    num = 0
                if num < 3:
                    l.append(post['Applicants'][i-1]['appId'])

    return render_template('Faculty_Evaluator.html',posts=posts,Id=facultyId,l=l)

@app.route('/student', methods=['GET', 'POST'] )
def Student():
    posts = db.posts
    studentId = request.form.get("stuID","");
    studentId = int(studentId) if studentId else ""
    l= []
    for post in posts.find({'Applicants':{'$exists': True }}):
        for i in range(posts.aggregate([{"$match":{"_id":post['_id']}},{'$unwind':"$Applicants"},{'$group':{'_id':"$_id", 'number':{'$sum':1}}}])['result'][0]['number']):
            for post2 in posts.find({"GrantType" : "Student" , "Applicants.appId" : post['Applicants'][i-1]['appId']}, { "Applicants.$.Evaluators" : 1 }):
                try:
                    num = len(post2["Applicants"][0]["Evaluators"])
                except:
                    num = 0
                if num < 3:
                    l.append(post['Applicants'][i-1]['appId'])
                    
    return render_template('Student_Evaluator.html',posts=posts,Id=studentId,l=l)

if __name__ == '__main__':
    app.run(host='0.0.0.0',debug=True)
    









